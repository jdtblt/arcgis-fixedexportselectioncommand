﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using ESRI.ArcGIS.Mapping.Controls;
using ESRI.ArcGIS.Mapping.Controls.Resources;
using ESRI.ArcGIS.Mapping.Core;
using ESRI.ArcGIS.Client;
using ESRI.ArcGIS.Client.Extensibility;
using ESRI.ArcGIS.Client.Toolkit;
using ESRI.ArcGIS.Client.Toolkit.Utilities;
using ESRI.ArcGIS.Client.FeatureService;


namespace FixedExportSelectionCommand.AddIns
{
    [Category("CategorySelection"), Description("ExportSelectionDescriptionFixed"), DisplayName("ExportSelectionFixedDisplayName"), Export(typeof(ICommand))]
    public class ExportSelectionFixedCommand : ExportSelectionCommand
    {
        private Dictionary<string, CodedValueDomain> _CodedValueDomains;
        internal Dictionary<string, CodedValueDomain> CodedValueDomains
        {
            get
            {
                if (_CodedValueDomains == null)
                {
                    _CodedValueDomains = new Dictionary<string, CodedValueDomain>();
                }
                return _CodedValueDomains;
            }
        }

        public override void Execute(object parameter)
        {
            if (base.Layer != null)
            {
                GraphicsLayer graphicsLayer = base.Layer as GraphicsLayer;
                
                if (graphicsLayer != null)
                {
                    try
                    {
                        SaveFileDialog dialog = new SaveFileDialog
                        {
                            DefaultExt = ".csv",
                            Filter = "CSV Files|*.csv|Text Files|*.txt|All Files|*.*"
                        };
                        if (dialog.ShowDialog() == true)
                        {
                            using (Stream stream = dialog.OpenFile())
                            {
                                StringBuilder builder = new StringBuilder();
                                bool flag = false; //used to build the header.
                                Collection<FieldInfo> fields = ESRI.ArcGIS.Mapping.Core.LayerExtensions.GetFields(graphicsLayer);

                                if (graphicsLayer is FeatureLayer)
                                {
                                    foreach (var f in ((FeatureLayer)graphicsLayer).LayerInfo.Fields)
                                    {
                                        if (f.Domain != null && !CodedValueDomains.ContainsKey(f.Domain.Name))
                                        {
                                            CodedValueDomains.Add(f.FieldName, f.Domain as CodedValueDomain);
                                        }
                                    }
                                }

                                if (fields != null)
                                {
                                    foreach (Graphic graphic in graphicsLayer.SelectedGraphics)
                                    {
                                        if (!flag)
                                        {
                                            foreach (string str in graphic.Attributes.Keys)
                                            {
                                                FieldInfo info = getField(str, fields);
                                                if ((info != null) && info.VisibleInAttributeDisplay)
                                                {
                                                    if (builder.Length > 0)
                                                    {
                                                        builder.Append(",");
                                                    }
                                                    builder.Append(info.DisplayName);
                                                }
                                            }
                                            builder.AppendLine();
                                            flag = true;
                                        }


                                        StringBuilder builder2 = new StringBuilder();

                                        foreach (KeyValuePair<string, object> pair in graphic.Attributes)
                                        {
                                            object obj2 = pair.Value;
                                            FieldInfo info2 = getField(pair.Key, fields);
                                            if ((info2 != null) && info2.VisibleInAttributeDisplay)
                                            {
                                                if (obj2 == null)
                                                {
                                                    builder2.Append(",");
                                                }
                                                else
                                                {
                                                    string str3 = Convert.ToString(obj2);
                                                    try
                                                    {
                                                        if (info2.DomainSubtypeLookup != DomainSubtypeLookup.None)
                                                        {
                                                            if (CodedValueDomains.ContainsKey(info2.Name))
                                                            {
                                                                str3 = CodedValueDomains[info2.Name].CodedValues[obj2];
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                       //ignore error
                                                    }
                                                    if (str3 == null)
                                                    {
                                                        str3 = string.Empty;
                                                    }
                                                    str3 = str3.Replace(",", "");
                                                    builder2.AppendFormat("{0},", new object[] { str3 });
                                                }
                                            }
                                        }

                                        string str4 = builder2.ToString();
                                        if (str4.Length > 0)
                                        {
                                            str4 = str4.Substring(0, str4.Length - 1);
                                        }
                                        if (!string.IsNullOrEmpty(str4))
                                        {
                                            builder.AppendLine(str4);
                                        }
                                    }
                                    byte[] bytes = Encoding.UTF8.GetBytes(builder.ToString());
                                    stream.Write(bytes, 0, bytes.Length);
                                }
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        string message = exception.Message;
                        MessageBoxDialog.Show("Error saving file", null, true);
                    }
                }
            }
        }

        private static FieldInfo getField(string key, Collection<FieldInfo> fields)
        {
            Func<FieldInfo, bool> predicate = null;
            if (fields != null)
            {
                if (predicate == null)
                {
                    predicate = f => f.Name == key;
                }
                FieldInfo info = fields.FirstOrDefault<FieldInfo>(predicate);
                if (info != null)
                {
                    return info;
                }
            }
            return null;
        }
    }
}
